package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import demo.WordCountMapper;
import demo.WordCountReducer;
import simple.mapred.SimpleMapReduce;
import simple.mapred.common.KeyValuePair;

public class SimpleMapReduceTest {
	
	@Test
	public void testMapRedWordCount() {
	    String testString = "This is green. This is blue";
	    List<String> values = Arrays.asList(testString.split("\\."));
	    
	    SimpleMapReduce<String, String, Integer, String, Integer> smr = 
	    		new SimpleMapReduce<String, String, Integer, String, Integer>(1, 1);
	    
	    WordCountMapper map = new WordCountMapper();
	    WordCountReducer reduce = new WordCountReducer();
	    @SuppressWarnings("unchecked")
		List<KeyValuePair<String, Integer>> result = 
	    		(List<KeyValuePair<String, Integer>>) smr.mapReduce(values, map, reduce);
	    
	    assertNotNull(result);
	    //checking  "This"
	    KeyValuePair<String, Integer> pair = getEntry("This", result);
	    assertNotNull(pair);
	    assertEquals(2, (int)pair.getValue());
	    
	    //checking "blue"
	    pair = getEntry("blue", result);
	    assertNotNull(pair);
	    assertEquals(1, (int)pair.getValue());
	    
	    //checking non-existent entry
	    pair = getEntry("a", result);
	    assertNull(pair);
	    
	}
	
	@Test
	public void testMapRedWordCountWith2MapworkersAnd2Reduceworkers() {
	    String testString = "This is green. This is blue. The day is over.";
	    List<String> values = Arrays.asList(testString.split("\\."));
	    
	    SimpleMapReduce<String, String, Integer, String, Integer> smr = 
	    		new SimpleMapReduce<String, String, Integer, String, Integer>(1, 1);
	    WordCountMapper map = new WordCountMapper();
	    WordCountReducer reduce = new WordCountReducer();
	    @SuppressWarnings("unchecked")
		List<KeyValuePair<String, Integer>> result = 
	    		(List<KeyValuePair<String, Integer>>) smr.mapReduce(values, map, reduce);
	    
	    assertNotNull(result);
	    //checking  "This"
	    KeyValuePair<String, Integer> pair = getEntry("This", result);
	    assertNotNull(pair);
	    assertEquals(2, (int)pair.getValue());
	    
	    //checking "blue"
	    pair = getEntry("blue", result);
	    assertNotNull(pair);
	    assertEquals(1, (int)pair.getValue());
	    
	    //checking non-existent entry
	    pair = getEntry("a", result);
	    assertNull(pair);
	    
	}
	
	@Test
	public void testMapOnlyWordCount() {
	    String testString = "This is green. This is blue";
	    List<String> values = Arrays.asList(testString.split("\\."));
	    
	    SimpleMapReduce<String, String, Integer, String, Integer> smr = 
	    		new SimpleMapReduce<String, String, Integer, String, Integer>(3, 0);
	    
	    WordCountMapper map = new WordCountMapper();
	    @SuppressWarnings("unchecked")
		List<KeyValuePair<String, Integer>> result = 
	    		(List<KeyValuePair<String, Integer>>) smr.mapReduce(values, map, null);
	    
	    assertNotNull(result);
	    //checking  "green"
	    KeyValuePair<String, Integer> pair = getEntry("green", result);
	    assertNotNull(pair);
	    assertEquals(1, (int)pair.getValue());
	    
	    //checking "blue"
	    pair = getEntry("blue", result);
	    assertNotNull(pair);
	    assertEquals(1, (int)pair.getValue());
	    
	    //checking non-existent entry
	    pair = getEntry("a", result);
	    assertNull(pair);
	    
	}

	
	@Test
	public void testMapRedWordCountWithNoInput() {
	    
		SimpleMapReduce<String, String, Integer, String, Integer> smr = 
	    		new SimpleMapReduce<String, String, Integer, String, Integer>(1, 1);
	    WordCountMapper map = new WordCountMapper();
	    WordCountReducer reduce = new WordCountReducer();
	    @SuppressWarnings("unchecked")
		List<KeyValuePair<String, Integer>> result = 
	    		(List<KeyValuePair<String, Integer>>) smr.mapReduce(new ArrayList<String>(), map, reduce);
	    assertTrue(result.isEmpty());
		
	}
	
	@Test
	public void testMapRedWordCountWithNulls() {
		String testString = "This is green. This is blue";
	    List<String> values = Arrays.asList(testString.split("\\."));
	    
		SimpleMapReduce<String, String, Integer, String, Integer> smr = 
	    		new SimpleMapReduce<String, String, Integer, String, Integer>(2, 1);
	    WordCountMapper map = new WordCountMapper();
	    WordCountReducer reduce = new WordCountReducer();
	    @SuppressWarnings("unchecked")
		List<KeyValuePair<String, Integer>> result = 
	    		(List<KeyValuePair<String, Integer>>) smr.mapReduce(null, null, null);
	    assertNull(result);
	}
	

	
	
/**
 * Helper method to retrieve value for key
 * @param key
 * @param result
 * @return
 */
	private KeyValuePair<String, Integer> getEntry(String key, List<KeyValuePair<String, Integer>> result) {
		for(KeyValuePair<String, Integer> pair : result) {
			if(pair.getKey().equals(key)) {
				return pair;
			}
		}
		return null;
	}
}
