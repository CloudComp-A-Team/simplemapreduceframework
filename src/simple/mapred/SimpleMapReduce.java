package simple.mapred;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import simple.mapred.common.Collector;
import simple.mapred.map.MapOperation;
import simple.mapred.reduce.ReduceOperation;


/**
 * Master for Map-Reduce framework.
 * 
 * Expects 5 generics which define type of input and output key/value pairs
 * First 1 defines the type of input values.
 * Next 2 define intermediate key-value pairs. (map output type)
 * Last 2 define final output key-value pairs. (reduce output type)
 * 
 * @author john
 *
 * @param <T> Input value type
 * @param <K> Map output key type
 * @param <V> Map output value type
 * @param <K2> Reduce output key type
 * @param <V2> Reduce output value type
 */
public class SimpleMapReduce<T, K, V, K2, V2> {
	private int NO_OF_MAP_WORKERS = 1;
	private int NO_OF_REDUCE_WORKERS = 1;
	
	private List<Worker> m_worker_pool;
	private List<Worker> r_worker_pool;
	
	public SimpleMapReduce(int m_workers, int r_workers) {
		if(m_workers > 0) {
			this.NO_OF_MAP_WORKERS = m_workers; //ensure at least 1 map worker
		}
		this.NO_OF_REDUCE_WORKERS = r_workers;
	}
    
    public List<?> mapReduce(List<T> values, MapOperation<T, K, V> map, ReduceOperation<K, V, K2, V2> reduce) {
		if(values == null || map == null) {
			return null;
		}
		
		if(reduce == null) {
			//assume a map-only work required
			this.NO_OF_REDUCE_WORKERS = 0;
		}
		
    	//Setup
    	setup();
    	
    	//Map Phase
    	Collector<K, V> mapOutputCollector = new Collector<K, V>(); 
    	
    	Map<Worker, List<T>> mapTaskDistribution = distributeTaskDataPerWorker(m_worker_pool, values);
    	for(Entry<Worker, List<T>> entry : mapTaskDistribution.entrySet()) {
    		Worker mapWorker = entry.getKey();
    		List<T> data = entry.getValue();
    		
    		Task<T, K, V> t = new Task<T, K, V>(map, data, mapOutputCollector);
    		mapWorker.submit(t);
    	}
    	   //execute workers
    	for(Worker w : m_worker_pool) {
    		w.execute();
    	}
    	
    	if(this.NO_OF_REDUCE_WORKERS < 1) {
    		//return map output since no reduce work (Map-only)
    		return mapOutputCollector.getItemsAsList();
    	}
    	
    	//Reduce Phase
    	Collector<K2, V2> outputCollector = new Collector<K2, V2>();
    	
    	int intermediateDataSize = mapOutputCollector.getItems().entrySet().size();
    	int maxTaskPerReduceWorker = (int) Math.ceil(intermediateDataSize * 1.0 / NO_OF_REDUCE_WORKERS);
    	int reduceWorkerSelector = 0;
    	int reduceWorkerTaskCounter = 0;
    	for(Entry<K, List<V>> entry : mapOutputCollector.getItems().entrySet()) {
    		K k = entry.getKey();
    		List<V> v = entry.getValue();
    		
    		Worker reduceWorker = r_worker_pool.get(reduceWorkerSelector);
    		
    		Task<T, K, V> t = new Task<T, K, V>(reduce, k, v, outputCollector);
    		reduceWorker.submit(t);	

    		reduceWorkerTaskCounter += 1;
    		
    		if(reduceWorkerTaskCounter >= maxTaskPerReduceWorker) {
    			reduceWorkerSelector += 1; //select next worker to assign tasks
    			reduceWorkerTaskCounter = 0; //reset task counter for worker
    		}
    	}
    	
    	//execute workers
    	for(Worker w : r_worker_pool) {
    		w.execute();
    	}
    	
    	return outputCollector.getItemsAsList();
	}
    
    private void setup() {
    	m_worker_pool = new ArrayList<Worker>();
    	for(int i=0; i < this.NO_OF_MAP_WORKERS; i ++ ) {
    		Worker worker = new Worker();
    		m_worker_pool.add(worker);
    	}
    	
    	r_worker_pool = new ArrayList<Worker>();
    	if(NO_OF_REDUCE_WORKERS > 0) {
    		for(int i=0; i<NO_OF_REDUCE_WORKERS; i++) {
    			Worker worker = new Worker();
    			r_worker_pool.add(worker);
    		}
    	}
    }
    
    private Map<Worker, List<T>> distributeTaskDataPerWorker(List<Worker> workers, List<T> values) {
    	
    	Map<Worker, List<T>> result = new HashMap<Worker, List<T>>();
    	
    	int maxChunkPerWorker = (int) Math.ceil(values.size() * 1.0 / workers.size());
    	if(maxChunkPerWorker < 1) {
    		//assign all to 1 worker
    		result.put(workers.get(0), values);
    		return result;
    	}
    	
    	int offset = 0;
    	for(Worker w : workers) {
    		int limit = offset + maxChunkPerWorker;
    		if(limit > values.size()) {
    			limit = values.size();
    		}
    		List<T> chunk = values.subList(offset, limit );
    		offset += maxChunkPerWorker;
    		result.put(w, chunk);
    	}
    	return result;
    }
 }

