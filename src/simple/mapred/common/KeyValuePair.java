package simple.mapred.common;
/**
 * Key-Value pair representation
 * 
 * @author john
 *
 * @param <K> Key type
 * @param <V> Value type
 */
public class KeyValuePair<K, V> {
	K key;
	V value;

	public KeyValuePair(K key, V value) {
		this.key = key;
		this.value = value;
	}

	public K getKey() {
		return key;
	}

	public V getValue() {
		return value;
	}
	
}
