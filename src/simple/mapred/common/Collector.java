package simple.mapred.common;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
/**
 * Container for storing output key-value pairs
 * 
 * @author john
 *
 * @param <K> Key type
 * @param <V> Value type
 */
public class Collector<K, V>{

	private Map<K, List<V>> items = new HashMap<K, List<V>>();
	
	public void write(K key, V value) {
		List<V> values = (List<V>) items.get(key);
		if(values == null) {
			values = new ArrayList<V>();
		}
		values.add(value);
		items.put(key, values);
	}
	
	public Map<K, List<V>> getItems() {
		return this.items;
	}
	
	public List<KeyValuePair<K, V>> getItemsAsList(){
		List<KeyValuePair<K, V>> result = new ArrayList<KeyValuePair<K, V>>();
		for(Entry<K, List<V>> entry : items.entrySet()) {
			K key = entry.getKey();
			List<V> values = entry.getValue();
			for(V value : values) {
				result.add(new KeyValuePair<K, V>(key, value));
			}
		}
		return result;
	}
}
