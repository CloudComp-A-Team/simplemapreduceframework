package simple.mapred;

import java.util.LinkedList;
import java.util.List;

public class Worker {
	private List<Task<?, ?, ?>> tasks = new LinkedList<Task<?, ?, ?>>();
	
	public void submit(Task<?, ?, ?> t) {
		tasks.add(t);
	}
	
	public void execute() {
		for(Task<?, ?, ?> t : tasks) {
			t.run();
		}
	}
}
