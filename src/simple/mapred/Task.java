package simple.mapred;
import java.util.List;

import simple.mapred.common.Collector;
import simple.mapred.map.MapOperation;
import simple.mapred.reduce.ReduceOperation;

/**
 * Represents a specific map/reduce task unit for execution
 * 
 * @author john
 *
 * @param <T> Map input value type
 * @param <K> Reduce input key type
 * @param <V> Reduce input value type
 */
public class Task<T, K, V> implements Runnable{
	Integer id;
	private Operation op;
	private List<T> mapInputData;
	private List<V> reduceInputData;
	private K key;
	
	private Collector<?, ?> c;
	
	public Task( Operation op, List<T> m_data, Collector<?, ?> c) {
		this.op = op;
		this.mapInputData = m_data;
		this.c = c;
	}
	
	public Task( Operation op, K key, List<V> r_data, Collector<?, ?> c) {
		this.op = op;
		this.key = key;
		this.reduceInputData = r_data;
		this.c = c;
	}
	
	@Override
	public void run() {
		if(op instanceof MapOperation) {
			for(T data : mapInputData) {
				((MapOperation)op).map(data, c);
			}
		}else {
			((ReduceOperation)op).reduce(key, reduceInputData, c);
		}
		
	}
}
