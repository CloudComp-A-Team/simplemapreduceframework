package simple.mapred.map;

import simple.mapred.common.Collector;
import simple.mapred.Operation;

/**
 * Interface for Map operations.
 * Expects 3 Generics which define type of input and output key-value pairs
 * First 1 define input value type, last 2 define intermediate output key-value pairs
 * 
 * @author john
 *
 * @param <T> Input value type
 * @param <K> Intermediate output key type
 * @param <V> Intermediate output value type
 */
public interface MapOperation<T, K, V> extends Operation {
	public void map(T value, Collector<K, V> c);
}
