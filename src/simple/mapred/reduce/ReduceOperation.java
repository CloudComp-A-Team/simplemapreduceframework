package simple.mapred.reduce;

import simple.mapred.Operation;
import simple.mapred.common.Collector;

/**
 * Interface for Reduce operations.
 * Expects 4 Generics which define type of input and output key-value pairs
 * First 2 define intermediate key-value pairs, last 2 define final output key-value pairs
 * 
 * @author john
 *
 * @param <K> Intermediate key type
 * @param <V> Intermediate value type
 * @param <K2> Output key type
 * @param <V2> Output value type
 */
public interface ReduceOperation<K, V, K2, V2> extends Operation {
	public void reduce(K key, Iterable<V> values, Collector<K2, V2> c);
}
