package demo;

import simple.mapred.common.Collector;
import simple.mapred.reduce.ReduceOperation;

public class WordCountReducer implements ReduceOperation<String, Integer, String, Integer>{

	@Override
	public void reduce(String key, Iterable<Integer> values, Collector<String, Integer> collector) {
		int wordCount = 0;
		for (Integer value : values) {
			wordCount += value;
		}
		collector.write(key, wordCount);
	}
	
}
