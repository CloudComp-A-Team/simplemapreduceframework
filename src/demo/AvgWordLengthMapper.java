package demo;

import java.util.Arrays;
import java.util.List;

import simple.mapred.common.Collector;
import simple.mapred.map.MapOperation;

public class AvgWordLengthMapper implements MapOperation<String, String, Integer> {
	private boolean caseSensitive = false;
	private static List<String> EXCLUSION_LIST = Arrays.asList("a", "an", "the");

	@Override
	public void map(String line, Collector<String, Integer> collector) {
		// take line as input
		// split word with regEx

		for (String word : line.split("\\W+")) {
			// if is not blank (double spaces etc)
			if (word.length() > 0 && !EXCLUSION_LIST.contains(word)) {
				if (!caseSensitive) {
					word = word.toLowerCase();
				}
				collector.write(word.substring(0, 1), word.length());
			}
		}

	}
}
