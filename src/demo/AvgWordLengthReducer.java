package demo;

import simple.mapred.common.Collector;
import simple.mapred.reduce.ReduceOperation;

public class AvgWordLengthReducer implements ReduceOperation<String, Integer, String, Double> {

	@Override
	public void reduce(String key, Iterable<Integer> values, Collector<String, Double> collector) {
		
		Double avg;
		int wordCount = 0;
		int letterCount = 0;
		for (Integer value : values) {
			wordCount++;
			letterCount += value;
		}

		avg = ((double) letterCount / wordCount);
		collector.write(key, avg);
	}

}
