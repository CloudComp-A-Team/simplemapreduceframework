package demo;

import simple.mapred.common.Collector;
import simple.mapred.map.MapOperation;

public class WordCountMapper implements  MapOperation<String, String, Integer> {

	@Override
	public void map( String line, Collector<String, Integer> collector) {
		for (String word : line.split("\\W+")) {
			//if is not blank (double spaces etc)
			if (word.length() > 0) {
				collector.write(word, 1);
			}
		}	
	}
}
