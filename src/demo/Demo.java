package demo;

import java.util.Arrays;
import java.util.List;

import simple.mapred.SimpleMapReduce;
import simple.mapred.common.KeyValuePair;

public class Demo {

	public static void main(String[] args) {
		// Test data
		String testString = "No one rejects, dislikes, or avoids pleasure itself, because it is pleasure"
				+ ", but because those who do not know how to pursue pleasure rationally encounter "
				+ "consequences that are extremely painful. Nor again is there anyone who loves or "
				+ "pursues or desires to obtain pain of itself. No one rejects, dislikes, or avoids "
				+ "pleasure itself, because it is pleasure, but because those who do not know how to "
				+ "pursue pleasure rationally encounter consequences that are extremely painful. "
				+ "Nor again is there anyone who loves or pursues or desires to obtain pain of itself. "
				+ "Because it is pain, but because occasionally circumstances occur in which toil and "
				+ "pain can procure him some great pleasure. To take a trivial example, which of us "
				+ "ever undertakes laborious physical exercise, except to obtain some advantage from it?"
				+ " But who has any right to find fault with a man who chooses to enjoy a pleasure that "
				+ "has no annoying consequences, or one who avoids a pain that produces no resultant "
				+ "pleasure? On the other hand, we denounce with righteous indignation and dislike men "
				+ "who are so beguiled and demoralized by the charms of pleasure of the moment, "
				+ "so blinded by desire, that they cannot foresee.";
				//"This is black. This is white . a an the .:,;? . : , ; ?";
		
		//Chunk data into list of lines/sentences 
		List<String> values = Arrays.asList(testString.split("\\."));
		
		SimpleMapReduce<String, String, Integer, String, Double> smr = 
	    		new SimpleMapReduce<String, String, Integer, String, Double>(3,2);
		
		//Create map & reduce operations
		AvgWordLengthMapper map = new AvgWordLengthMapper();
		AvgWordLengthReducer reduce = new AvgWordLengthReducer();
		
		//submit map-reduce job
		@SuppressWarnings("unchecked")
		List<KeyValuePair<String, Double>> result = 
				(List<KeyValuePair<String, Double>>) smr.mapReduce(values, map, reduce);
		
		//print results
		for(KeyValuePair<String, Double> t : result) {
			System.out.println(t.getKey() + " " + t.getValue());
			
		}
	
	}

}
