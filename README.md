**A minimal Map-Reduce framework**

---

## Background

Map-Reduce is a programming model introduced by Google in 2004 for parallel processing of large datasets on thousands of commodity machines.

Typically the model works by providing a 'map' function that processes a key/value pair to generate a set of intermediate key/value pairs, and a 'reduce' function that merges all intermediate values associated with the same intermediate key. The model provides automatic parallelization and distribution, fault-tolerance and I/O Scheduling.

---

## Basics

A Map-Reduce job requires two user-defined operations:

    - map (unary operator): T -> (K, V)
    - reduce (associative binary operator): V, V -> V

The Map-Reduce framework takes a dataset, i.e., a list of data items, and performs the map step, i.e., it applies the map operation on each data item which transforms it into a key-value pair. Afterwards, it performs the reduce step that applies the reduce operation keywise on all values with the same key.

Example:

    To count occurrences of words in a list of words, we may define the following operators:

    map operator: word: string -> (word: string, 1: integer)
    reduce operator: x: integer, y: integer -> x + y: integer

    The Map-Reduce framework applies the operators on a list of words and transforms it as follows:

    ['walter','arne','walter'] - map => [('walter', 1), ('arne', 1), ('walter', 1)] - reduce => [('walter', 2), ('arne', 1)]

---

## Problem

1. Model a simple worker that processes tasks (sequentially). Task and worker should provide the following methods:

    Worker:
    - submit(Task t) // Submits a task to the worker for later execution.
    - execute() // Executes all tasks that have been submitted.

    Task:
    - run() // Performs some user defined work.

2. Implement a master for our Map-Reduce framework that is initialized to provide some number of workers and performs a Map-Reduce in a way that user-defined operations are executed as tasks by the workers. The master should provide a method similar to this:

    - mapReduce(List<T> values, MapOperation map, ReduceOperation reduce): List<K, V>

The master distributes tasks to workers in order to perform the Map-Reduce. 

---

## Testing

3. Test implementation and consider corner test cases.

4. Test with an implementation to count word occurences in a text (i.e: *WordCountMapper.java* & *WordCountReducer.java*)

5. Test with an implementation to calculate the **average length of words** in a text (i.e: *AvgWordLengthMapper.java* & *AvgWordLengthReducer.java*)

